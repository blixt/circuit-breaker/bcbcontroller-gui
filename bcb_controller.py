import sys
import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import multiprocessing
import threading
import argparse
import time
import json
import logging
import slipdev
import serialio
import controller_ui

logger = logging.getLogger(__name__)

class ControllerApp(object):

    def __init__(self, config):
        self.config = config
        self.data_batch_size = config.get('data_batch_size')

        self.app = QtGui.QApplication([])
        self.app.aboutToQuit.connect(self._exit_handler) 
        styleSheet = ""
        styleSheet += open("style/style.qss").read()
        styleSheet += open("bcb_controller.qss").read()
        self.app.setStyleSheet(styleSheet)
        self.controller_ui = controller_ui.ControllerUI(self.config)

    def run(self):
        self.controller_ui.show()
        sys.exit(self.app.exec_()) 

    def _exit_handler(self):
        self.controller_ui.shutdown()
        QtCore.QCoreApplication.exit(0)


def init_config(config):

    confi_file = open("config.json")
    config.update(json.load(confi_file))
    confi_file.close()
    logger.debug("CONFIG %s", str(config))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Blixt Circuit Breaker Controller')
    parser.add_argument('-p', '--port', type=str,
                        default=None, help='Serial port')
    parser.add_argument('-b', '--baudrate', type=int, default=115200,
                        help='Baudrate of the serial port')
    parser.add_argument('-d', '--debug', action='store_true',
                        default=False, help='Debug output')
    parser.add_argument('-A', '--advanced', action='store_true',
                        default=False, help='Show advanced controls')

    args = parser.parse_args()
    config = vars(args)

    if config['debug'] is True:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    init_config(config)

    app = ControllerApp(config)
    app.run()
