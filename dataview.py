import numpy
import pyqtgraph
import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets
import utilities
import dataframe


class ChannelView(object):
    def __init__(self, config, vb, **kwargs):
        self.config = config
        self.vb = vb
        # Live view
        self.buf_lv_ptr = 0
        self.buf_size_lv = kwargs.get('buf_size_lv', 1000)
        self.buf_lv = numpy.empty((self.buf_size_lv, 2))
        # Final view
        self.buf_fv_ptr = 0
        self.buf_size_fv = kwargs.get('buf_size_fv', 100000)
        self.buf_fv = numpy.empty((self.buf_size_fv, 2))
        self.fv_enabled = False
        # Others
        self.pen = pyqtgraph.mkPen(color=kwargs.get('color', '#ffffff'))
        #self.plot_curv_item = pyqtgraph.PlotDataItem(pen=self.pen)
        #self.vb.addItem(self.plot_curv_item)

    def enable_fv(self, enabled=True):
        self.buf_fv_ptr = 0
        self.fv_enabled = enabled

    def add_data(self, time_tick, value):
        if self.buf_lv_ptr < self.buf_size_lv:
            self.buf_lv[self.buf_lv_ptr, 0] = time_tick
            self.buf_lv[self.buf_lv_ptr, 1] = value
            self.buf_lv_ptr += 1
        else:
            self.buf_lv[:-1, 0] = self.buf_lv[1:, 0]
            self.buf_lv[:-1, 1] = self.buf_lv[1:, 1]
            self.buf_lv[-1, 0] = time_tick
            self.buf_lv[-1, 1] = value

        if self.fv_enabled and self.buf_fv_ptr < self.buf_size_fv:
            self.buf_fv[self.buf_fv_ptr, 0] = time_tick
            self.buf_fv[self.buf_fv_ptr, 1] = value
            self.buf_fv_ptr += 1

    def is_in_fifo_mode(self):
        if self.buf_lv_ptr < self.buf_size_lv:
            return False
        else:
            return True

    def get_tick_range_lv(self):
        if self.buf_lv_ptr < 2:
            return (0, 0)
        else:
            return (self.buf_lv[0, 0], self.buf_lv[self.buf_lv_ptr-1, 0])

    def get_rms_lv(self):
        if self.buf_lv_ptr < 2:
            return 0.0
        else:
            return numpy.sqrt(numpy.mean(self.buf_lv[:self.buf_lv_ptr-1, 1]**2))

    def get_avg_lv(self):
        if self.buf_lv_ptr < 2:
            return 0.0
        else:
            return numpy.mean(self.buf_lv[:self.buf_lv_ptr-1, 1])

    def get_min_max_lv(self):
        if self.buf_lv_ptr < 2:
            return (0.0, 0.0)
        else:
            return (numpy.min(self.buf_lv[:self.buf_lv_ptr-1, 1]),
                    numpy.max(self.buf_lv[:self.buf_lv_ptr-1, 1]))

    def get_tick_range_fv(self):
        if self.buf_fv_ptr < 2:
            return (0, 0)
        else:
            return (self.buf_fv[0, 0], self.buf_fv[self.buf_fv_ptr-1, 0])

    def get_buf_status_fv(self):
        return (self.buf_size_fv, self.buf_fv_ptr)

    def clear(self):
        self.buf_lv_ptr = 0
        self.buf_fv_ptr = 0
        #self.plot_curv_item.clear()
        self.vb.clear()

    def update_lv(self):
        self.vb.setData(x=self.buf_lv[:self.buf_lv_ptr, 0],
                     y=self.buf_lv[:self.buf_lv_ptr, 1], pen=self.pen)

    def update_fv(self):
        #self.plot_curv_item.clear()
        self.vb.setData(x=self.buf_fv[:self.buf_fv_ptr, 0],
                     y=self.buf_fv[:self.buf_fv_ptr, 1], pen=self.pen)


class MovableBoxItem(pyqtgraph.GraphicsWidget, pyqtgraph.GraphicsWidgetAnchor):

    def __init__(self, size=None, offset=None):

        pyqtgraph.GraphicsWidget.__init__(self)
        pyqtgraph.GraphicsWidgetAnchor.__init__(self)
        self.setFlag(self.ItemIgnoresTransformations)
        self.layout = QtGui.QGraphicsGridLayout()
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.items = []
        self.size = size
        self.offset = offset
        if size is not None:
            self.setGeometry(QtCore.QRectF(0, 0, self.size[0], self.size[1]))

    def setParentItem(self, p):
        ret = pyqtgraph.GraphicsWidget.setParentItem(self, p)
        if self.offset is not None:
            offset = pyqtgraph.Point(self.offset)
            anchorx = 1 if offset[0] <= 0 else 0
            anchory = 1 if offset[1] <= 0 else 0
            anchor = (anchorx, anchory)
            self.anchor(itemPos=anchor, parentPos=anchor, offset=offset)
        return ret

    def addLabelItem(self, **kwargs):
        label = pyqtgraph.LabelItem(**kwargs)
        row = self.layout.rowCount()
        self.items.append(label)
        self.layout.addItem(label, row, 0)
        self.updateSize()
        return label

    def updateSize(self):
        if self.size is not None:
            return
        height = 0
        width = 0
        for label in self.items:
            height += label.height() + 3
            width = max(width, label.sizeHint(QtCore.Qt.MinimumSize, label.size()).width())
        self.setGeometry(0, 0, width+25, height)

    def boundingRect(self):
        return QtCore.QRectF(0, 0, self.width(), self.height())

    def paint(self, p, *args):
        #p.setPen(QtGui.QPen(QtGui.QColor(255, 255, 255, 0)))
        p.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 100, 200)))
        p.drawRect(self.boundingRect())

    def hoverEvent(self, ev):
        ev.acceptDrags(QtCore.Qt.LeftButton)

    def mouseDragEvent(self, ev):
        if ev.button() == QtCore.Qt.LeftButton:
            dpos = ev.pos() - ev.lastPos()
            self.autoAnchor(self.pos() + dpos)


class TimeAxisItem(pyqtgraph.AxisItem):

    def tickStrings(self, values, scale, spacing):
        """This is an overridden function of pyqtgraph.AxisItem.
        See its documentation for more details
        """

        # NOTE: We have to force the scale to be 1e-9 since pyqtgraph changes the scale
        # dynamically. This is only a quick fix and a proper solution should be used.
        scale = 1e-9
        # Calculate number of decimal places
        places = max(0, numpy.ceil(-numpy.log10(spacing*scale)))
        strings = []

        #print("---------- DEBUG ----------")
        #print("VALUES: ", values)
        #print("SCALE: ", scale)
        #print("SPACING: ", spacing)
        #print("PLACES: ", places)

        for v in values:
            vs = v * scale
            vstr = ""
            val_f, val_i = numpy.modf(vs)

            tick_h = int(val_i / 3600)
            tick_m = int((val_i % 3600) / 60)
            tick_s = int(((val_i % 3600) % 60) % 60)

            tick_hms_str = "%d s" % tick_s

            if tick_m != 0:
                tick_hms_str = "%d m: %d s" % (tick_m, tick_s)
            if tick_h != 0:
                tick_hms_str = "%d h: %d m: %d s" % (tick_h, tick_m, tick_s)

            if places > 0:
                # We have decimal places
                multipliers = [1, 100, 10]
                val_ = numpy.round(val_f * 10**places % 10) * multipliers[int(places) % 3]
                if places <= 3:
                    unit_ = "ms"
                elif places <= 6:
                    unit_ = "us"
                elif places <= 9:
                    unit_ = "ns"
                else:
                    unit_ = ""

                if len(values) < 4:
                    # few tick values. force to include full time stamp
                    val_ = 0

                if val_ == 0:
                    val_ = numpy.round(val_f * 10**places) * multipliers[int(places) % 3]
                    vstr += "%s: %d %s" % (tick_hms_str, val_, unit_)
                else:
                    vstr += "%d %s" % (val_, unit_)
            else:
                # we don't have decimal places
                vstr += "%s" % (tick_hms_str)

            strings.append(vstr)

        #print("STRINGS: ", strings)

        return strings

    def tickSpacing(self, minVal, maxVal, size):
        """This is an overridden function of pyqtgraph.AxisItem.
        See its documentation for more details
        """
        # First check for override tick spacing
        if self._tickSpacing is not None:
            return self._tickSpacing

        dif = abs(maxVal - minVal)
        if dif == 0:
            return []

        # decide optimal minor tick spacing in pixels (this is just aesthetics)
        optimalTickCount = max(2., numpy.log(size))
        # optimal minor tick spacing
        optimalSpacing = dif / optimalTickCount
        # the largest power-of-10 spacing which is smaller than optimal
        p10unit = 10 ** numpy.floor(numpy.log10(optimalSpacing))
        # Determine major/minor tick spacings which flank the optimal spacing.
        intervals = numpy.array([1., 10., 100.]) * p10unit
        minorIndex = 0
        while intervals[minorIndex+1] <= optimalSpacing:
            minorIndex += 1

        levels = [
            (intervals[minorIndex+1], 0),
            (intervals[minorIndex+1], 0),
            # (intervals[minorIndex], 0)  # Pretty, but eats up CPU
        ]

        if self.style['maxTickLevel'] >= 2:
            # decide whether to include the last level of ticks
            minSpacing = min(size / 20., 30.)
            maxTickCount = size / minSpacing
            if dif / intervals[minorIndex] <= maxTickCount:
                levels.append((intervals[minorIndex], 0))

            return levels

    def labelString(self):
        style = ';'.join(['%s: %s' % (k, self.labelStyle[k]) for k in self.labelStyle])
        return ("<span style='%s'>%s</span>") % (style, self.labelText)


class MultiChannelViewPlot(object):
    def __init__(self, config, **kwargs):
        self.config = config
        time_axis_item = TimeAxisItem(orientation='bottom')
        self.plt_widget = pyqtgraph.PlotWidget(axisItems={'bottom': time_axis_item})
        self.plt_item = self.plt_widget.getPlotItem()
        self.vbs = []
        self.y_axis_items = []
        self.plt_widget.showGrid(x=True, y=True, alpha=1)
        self.plt_widget.getViewBox().sigResized.connect(self._update_vbs)

    def add_channel(self, **kwargs):
        vb = self._add_y_axis(**kwargs)
        return ChannelView(self.config, self.plt_widget.plot(), **kwargs)

    def set_x_axis(self, **kwargs):
        if kwargs.get('label', False):
            self.plt_item.getAxis('bottom').setLabel(kwargs.get('label', False))
        if kwargs.get('color', False):
            self.plt_item.getAxis('bottom').setLabel(color=kwargs.get('color', '#ffffff'))

    def link_x_axis(self, multi_view_plot):
        self.plt_item.setXLink(multi_view_plot.plt_item)

    def set_limits(self, auto_range=False, **kwargs):
        self.plt_widget.getViewBox().setLimits(**kwargs)
        if auto_range:
            self.plt_widget.getViewBox().autoRange()

    def get_widget(self):
        return self.plt_widget

    def _add_y_axis(self, **kwargs):
        if len(self.vbs) == 0:
            viewbox = self.plt_widget.getViewBox()
            y_axis_item = self.plt_item.getAxis('left')
            y_axis_item.setWidth(50)
        else:
            viewbox = pyqtgraph.ViewBox()
            y_axis_item = pyqtgraph.AxisItem('right')
            y_axis_item.setWidth(50)
            self.plt_item.layout.addItem(y_axis_item, 2, 3 + len(self.y_axis_items))
            self.plt_item.scene().addItem(viewbox)
            y_axis_item.linkToView(viewbox)
            viewbox.setXLink(self.plt_item)
            y_axis_item.setZValue(-10000)

        if kwargs.get('label', False):
            y_axis_item.setLabel(kwargs.get('label', False))
        if kwargs.get('color', False):
            y_axis_item.setLabel(color=kwargs.get('color', '#ffffff'))

        self.vbs.append(viewbox)
        self.y_axis_items.append(y_axis_item)

        return viewbox

    def _update_vbs(self):
        for vb in self.vbs:
            if vb == self.plt_widget.getViewBox():
                continue
            else:
                vb.setGeometry(self.plt_widget.getViewBox().sceneBoundingRect())
                vb.linkedXChanged()


class IVDataView(object):
    def __init__(self, config):
        self.config = config
        self.ch_views = {}
        self.plots = []
        self.min_x_rage = 1000000

        self.plot_i = MultiChannelViewPlot(self.config)
        self.plot_i.set_x_axis(label='Time (S)')
        self.plot_i.set_x_axis(color='#FFFFFF')
        self.plot_i.set_limits(minYRange=0.5, minXRange=self.min_x_rage)
        self.plots.append(self.plot_i)

        buf_size_lv = utilities.get_config(self.config, 1000, 'buf_size_lv')
        buf_size_fv = utilities.get_config(self.config, 1000, 'buf_size_fv')

        ch_config = utilities.get_config(self.config, {}, "dataviews", "current")
        label = utilities.get_config(ch_config, "A", "label")
        color = utilities.get_config(ch_config, "#FFFFFF", "color")
        self.ch_view_i = self.plot_i.add_channel(color=color, label=label,
                                                 buf_size_lv=buf_size_lv, buf_size_fv=buf_size_fv)

        self.plot_v = MultiChannelViewPlot(self.config)
        self.plot_v.set_x_axis(label='Time (S)')
        self.plot_v.set_x_axis(color='#FFFFFF')
        self.plot_v.set_limits(minYRange=40, minXRange=self.min_x_rage)
        self.plots.append(self.plot_v)

        ch_config = utilities.get_config(self.config, {}, "dataviews", "voltage")
        label = utilities.get_config(ch_config, "V", "label")
        color = utilities.get_config(ch_config, "#FFFFFF", "color")
        self.ch_view_v = self.plot_v.add_channel(color=color, label=label,
                                                 buf_size_lv=buf_size_lv, buf_size_fv=buf_size_fv)

        self.plot_i.link_x_axis(self.plot_v)

        self.mbox_i = MovableBoxItem(offset=(-1, 1))
        self.mbox_i.setParentItem(self.plot_i.get_widget().getViewBox())
        self.text_box_i_rms = self.mbox_i.addLabelItem()
        self.text_box_i_p2p = self.mbox_i.addLabelItem()
        self.text_box_i_avg = self.mbox_i.addLabelItem()

        self.mbox_v = MovableBoxItem(offset=(-1, 1))
        self.mbox_v.setParentItem(self.plot_v.get_widget().getViewBox())
        self.text_box_v_rms = self.mbox_v.addLabelItem()
        self.text_box_v_p2p = self.mbox_v.addLabelItem()
        self.text_box_v_avg = self.mbox_v.addLabelItem()

    def enable_fvs(self, enabled=True):
        self.ch_view_i.enable_fv(enabled)
        self.ch_view_v.enable_fv(enabled)

    def clear(self):
        self.ch_view_i.clear()
        self.ch_view_v.clear()

    def add_data(self, time_tick, current, voltage):
        self.ch_view_i.add_data(time_tick, current)
        self.ch_view_v.add_data(time_tick, voltage)

    def update_lvs(self):
        self.ch_view_i.update_lv()
        self.ch_view_v.update_lv()
        x_min, x_max = self.ch_view_i.get_tick_range_lv()

        self.text_box_i_rms.setText("RMS %.3f" % (round(self.ch_view_i.get_rms_lv(), 3)))
        y_i_min, y_i_max = self.ch_view_i.get_min_max_lv()
        self.text_box_i_p2p.setText("P2P %.3f" % (round((y_i_max - y_i_min), 3)))
        self.text_box_i_avg.setText("AVG %.3f" % (round(self.ch_view_i.get_avg_lv(), 3)))

        self.text_box_v_rms.setText("RMS %.3f" % (round(self.ch_view_v.get_rms_lv(), 3)))
        y_v_min, y_v_max = self.ch_view_v.get_min_max_lv()
        self.text_box_v_p2p.setText("P2P %.3f" % (round((y_v_max - y_v_min), 3)))
        self.text_box_v_avg.setText("AVG %.3f" % (round(self.ch_view_v.get_avg_lv(), 3)))

        if self.ch_view_i.is_in_fifo_mode():
            self.plot_i.set_limits(auto_range=False, xMin=x_min, xMax=x_max)
            self.plot_v.set_limits(auto_range=False, xMin=x_min, xMax=x_max)
        else:
            self.plot_i.set_limits(auto_range=True, xMin=x_min, xMax=None)
            self.plot_v.set_limits(auto_range=True, xMin=x_min, xMax=None, yMin=None, yMax=None)

    def update_fvs(self):
        self.ch_view_i.update_fv()
        self.ch_view_v.update_fv()
        x_min, x_max = self.ch_view_i.get_tick_range_fv()
        self.plot_i.set_limits(auto_range=True, xMin=x_min, xMax=x_max)
        self.plot_v.set_limits(auto_range=True, xMin=x_min, xMax=x_max)

    def get_widgets(self):
        return [plot.get_widget() for plot in self.plots]

    def get_buf_status_fv(self):
        return self.ch_view_i.get_buf_status_fv()
