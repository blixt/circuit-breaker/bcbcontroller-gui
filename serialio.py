import PyQt5.QtCore as QtCore
import threading
import multiprocessing
import signal
import serial
import random
import time
import logging
import slipdev
import dataframe
import msgpack
import socket

class SerialReadThread(threading.Thread):

    def __init__(self, shutdown_event, slip_dev, serial_r_pipe_in, serial_r_err_pipe_in):
        super().__init__()
        self.slip_dev = slip_dev
        self.shutdown_event = shutdown_event
        self.serial_r_pipe_in = serial_r_pipe_in
        self.serial_r_err_pipe_in = serial_r_err_pipe_in
        self.logger = logging.getLogger(self.__class__.__name__)
        self.packer = msgpack.Packer()

    def run(self):
        self.logger.debug('In SerialReadThread')
        while not self.shutdown_event.is_set():
            try:
                while not self.shutdown_event.is_set():
                    slip_frame = self.slip_dev.read()
                    if len(slip_frame) > 0:
                        packed_data = msgpack.packb(slip_frame)
                        self.serial_r_pipe_in.send(packed_data)

            except dataframe.FrameDecodeError as e:
                self.logger.warn(e)
            except Exception as e:
                self.logger.error(e)
                self.shutdown_event.set()
                self.serial_r_err_pipe_in.send(e)


class SerialWriteThread(threading.Thread):

    def __init__(self, shutdown_event, slip_dev, serial_w_pipe_out, serial_w_err_pipe_in):
        super().__init__()
        self.slip_dev = slip_dev
        self.shutdown_event = shutdown_event
        self.serial_w_pipe_out = serial_w_pipe_out
        self.serial_w_err_pipe_in = serial_w_err_pipe_in
        self.logger = logging.getLogger(self.__class__.__name__)
        self.unpacker = msgpack.Unpacker()

    def run(self):
        self.logger.debug('In SerialWriteThread')
        while not self.shutdown_event.is_set():
            try:
                while not self.shutdown_event.is_set():
                    if self.serial_w_pipe_out.poll(0.01):
                        bytes_recv = self.serial_w_pipe_out.recv()
                        self.unpacker.feed(bytes_recv)
                        try:
                            unpacked_frame = self.unpacker.unpack()
                            self.slip_dev.write(unpacked_frame)
                        except Exception as e:
                            continue
                    else:
                        time.sleep(0.01)
            except Exception as e:
                self.logger.error(e)
                self.shutdown_event.set()
                self.serial_w_err_pipe_in.send(e)


class SerialIO(object):
    def __init__(self, **kwargs):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.read_batch_size = kwargs.get('read_batch_size', 1000)
        self.data_callback = None
        self.shutdown_event = threading.Event()
        self.serial_r_pipe_in, self.serial_r_pipe_out = multiprocessing.Pipe()
        self.serial_w_pipe_in, self.serial_w_pipe_out = multiprocessing.Pipe()
        self.serial_r_err_pipe_in, self.serial_r_err_pipe_out = multiprocessing.Pipe()
        self.serial_w_err_pipe_in, self.serial_w_err_pipe_out = multiprocessing.Pipe()
        self.slip_dev = None
        self.read_thread = None
        self.write_thread = None
        self.unpacker = msgpack.Unpacker()

    def connect(self, port, baudrate, **kwargs):
        self.shutdown_event.set()

        if not self.read_thread is None:
            self.read_thread.join()

        if not self.write_thread is None:
            self.write_thread.join()
        self.shutdown_event.clear()

        if not self.slip_dev is None:
            self.slip_dev.close()

        try:
            self.slip_dev = slipdev.SLIPDevice(port=port,
                                               baudrate=baudrate,
                                               timeout=1,
                                               rtscts=0,
                                               xonxoff=0)
            self.slip_dev.flushInput()
            self.slip_dev.flushOutput()
        except Exception as e:
            self.logger.error(e)
            self._slip_dev = None
            raise Exception(e)
        self.read_thread = SerialReadThread(self.shutdown_event,
                                            self.slip_dev,
                                            self.serial_r_pipe_in,
                                            self.serial_r_err_pipe_in)

        self.write_thread = SerialWriteThread(self.shutdown_event,
                                              self.slip_dev,
                                              self.serial_w_pipe_out,
                                              self.serial_w_err_pipe_in)

        self.read_thread.start()
        self.write_thread.start()

    def disconnect(self):
        self.shutdown_event.set()

        if self.read_thread is not None:
            self.logger.debug('waiting SerialReadThread')
            self.read_thread.join()

        if self.write_thread is not None:
            self.logger.debug('waiting SerialWriteThread')
            self.write_thread.join()
        self.shutdown_event.clear()

        if not self.slip_dev is None:
            self.slip_dev.close()

        self.slip_dev = None
        self.read_thread = None
        self.write_thread = None

    def set_data_callback(self, callback):
        self.data_callback = callback

    def send(self, data_frame):
        if self.write_thread is not None and self.write_thread.is_alive():
            packed_data = msgpack.packb(data_frame.pack())
            self.serial_w_pipe_in.send(packed_data)

    def poll(self):
        i = 0
        while (i < self.read_batch_size and self.serial_r_pipe_out.poll()):
            bytes_recv = self.serial_r_pipe_out.recv()
            self.unpacker.feed(bytes_recv)
            try:
                unpacked_frame = self.unpacker.unpack()
                data_frame = dataframe.create_frame(unpacked_frame)
                if self.data_callback is not None:
                    self.data_callback(data_frame)
                i += 1
            except Exception as e:
                self.logger.error(e)
                raise Exception(e)

        if self.serial_r_err_pipe_out.poll():
            e = self.serial_r_err_pipe_out.recv()
            self.logger.error(e)
            raise Exception(e)

        if self.serial_w_err_pipe_out.poll():
            e = self.serial_w_err_pipe_out.recv()
            self.logger.error(e)
            raise Exception(e)

    def set_dtr(self, value=1):
        self.slip_dev.setDTR(value)

    def set_rts(self, value=1):
        self.slip_dev.setRTS(value)

    @staticmethod
    def get_ports(hwid_filter=[]):
        return slipdev.SLIPDevice.get_ports(hwid_filter)
