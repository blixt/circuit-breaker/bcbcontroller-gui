import enum
import numpy
import struct
import abc
import logging


class FrameDecodeError(Exception):
    pass


class DataFrameType(enum.IntEnum):
    NONE = 0
    DEBUG = 1
    ADC = 2
    # Commands
    COMMAND_ON = 20
    COMMAND_OFF = 21
    COMMAND_OCP_OPT_RESET = 22
    COMMAND_CALIBRATE = 23
    COMMAND_GET_STATUS = 24
    COMMAND_GET_CONFIG = 25
    COMMAND_START_ADC_DUMP = 26
    COMMAND_STOP_ADC_DUMP = 27
    # Responses
    REPONSE_STATUS = 40
    REPONSE_CONFIG = 41


class DataFrame(abc.ABC):
    def __init__(self):
        pass

    def pack(self):
        pass

    @staticmethod
    def unpack(bytes):
        pass


class CommandFrameNoArgs(DataFrame):
    def __init__(self, frame_type):
        super(CommandFrameNoArgs, self).__init__()
        self.frame_type = frame_type

    def pack(self):
        data_bytes = bytearray()
        data_bytes.append(int(self.frame_type.value) & 0xFF)
        return data_bytes


class DebugPrintFrame(DataFrame):

    def __init__(self):
        super(DebugPrintFrame, self).__init__()
        self.data = ""

    def __str__(self):
        return self.data

    def pack(self):
        data_bytes = bytearray()
        data_bytes.extend(struct.pack("<B", int(DataFrameType.DEBUG.value)))
        data_bytes.extend(map(ord, self.data))
        return data_bytes

    @staticmethod
    def unpack(bytes):
        """
            |type(1)|ascii string|
        """
        frame_type, = struct.unpack("<B", bytes[:1])
        if frame_type != DataFrameType.DEBUG.value:
            raise FrameDecodeError("Invalid frame type %d" % frame_type)

        print_frame = DebugPrintFrame()
        print_frame.data = ''.join([chr(i) if i < 128 else '.' for i in bytes[1:]])
        return print_frame


class ConfigFrame(DataFrame):

    def __init__(self):
        super(ConfigFrame, self).__init__()
        self.c1_zero = 0
        self.c2_zero = 0
        self.v_zero = 0

    @staticmethod
    def unpack(bytes):
        """
            |type(1)|current1_zero(2)|current2_zero(2)|voltage_zero(2)|
        """
        frame_type, = struct.unpack("<B", bytes[:1])
        if frame_type != DataFrameType.REPONSE_CONFIG.value:
            raise FrameDecodeError("Invalid frame type %d. Expects %d" % frame_type,
                                   DataFrameType.REPONSE_CONFIG.value)
        if len(bytes) != 7:
            raise FrameDecodeError("Invalid frame length %d. Expects %d" % (len(bytes), 7))

        config_frame = ConfigFrame()
        config_frame.c1_zero, = struct.unpack("<H", bytes[1:3])
        config_frame.c2_zero, = struct.unpack("<H", bytes[3:5])
        config_frame.v_zero, = struct.unpack("<H", bytes[5:7])
        return config_frame


class StatusFrame(DataFrame):
    def __init__(self):
        super(StatusFrame, self).__init__()
        self.is_on = False
        self.is_power_good = False
        self.is_ocp_triggered = False
        self.is_otp_triggered = False
        self.is_adc_dump_active = False

    def __str__(self):
        line = "On %s, " % self.is_on
        line += "Power Good %s, " % self.is_power_good
        line += "OTP Triggered %s, " % self.is_otp_triggered
        line += "OCP Triggered %s" % self.is_ocp_triggered
        return line

    def pack(self):
        data_bytes = bytearray()
        data_bytes.extend(struct.pack("<B", int(DataFrameType.REPONSE_STATUS.value)))
        return data_bytes

    @staticmethod
    def unpack(bytes):
        frame_type, = struct.unpack("<B", bytes[:1])
        if frame_type != DataFrameType.REPONSE_STATUS.value:
            raise FrameDecodeError("Invalid frame type %d. Expects %d" % frame_type,
                                   DataFrameType.REPONSE_CONFIG.value)
        if len(bytes) != 2:
            raise FrameDecodeError("Invalid frame length %d. Expects %d" % (len(bytes), 2))

        status_frame = StatusFrame()
        status_frame.is_on = (bytes[1] & 0x01 == 0x01)
        status_frame.is_power_good = (bytes[1] & 0x02 == 0x02)
        status_frame.is_ocp_triggered = (bytes[1] & 0x04 == 0x04)
        status_frame.is_otp_triggered = (bytes[1] & 0x08 == 0x08)
        status_frame.is_adc_dump_active = (bytes[1] & 0x10 == 0x10)
        return status_frame


class ADCDataFrame(DataFrame):
    def __init__(self):
        super(ADCDataFrame, self).__init__()
        self.time_tick = 0
        self.channel_values = []
        self.adc_c1 = 0
        self.adc_c2 = 0
        self.adc_v = 0

    def pack(self):
        string = struct.pack("<B", DataFrameType.ADC.value)
        string += struct.pack("<L", self.time_tick)
        for channel, value in self.channel_values:
            string += struct.pack("<H", channel, value)
        return string

    @staticmethod
    def unpack(bytes):
        """
            |type(1)|time_tick(4)|value(2)|value(2)|value(2)
        """
        frame_type, = struct.unpack("<B", bytes[:1])
        if frame_type != DataFrameType.ADC.value:
            raise FrameDecodeError("Invalid frame type %d. Expects %d" % frame_type,
                                   DataFrameType.ADC.value)
        if len(bytes) != 11:
            raise FrameDecodeError("Invalid frame length %d. Expects %d" % (len(bytes), 11))

        adc_frame = ADCDataFrame()
        adc_frame.time_tick, = struct.unpack("<L", bytes[1:5])
        ch_id = 1
        for i in numpy.arange(5, len(bytes), 2):
            adc_value, = struct.unpack("<H", bytes[i:i+2])
            adc_frame.channel_values.append((ch_id, adc_value))
            ch_id += 1
        return adc_frame


def create_frame(data_bytes):
    frame_type, = struct.unpack("<B", data_bytes[:1])
    if frame_type == int(DataFrameType.DEBUG):
        return DebugPrintFrame.unpack(data_bytes)
    elif frame_type == int(DataFrameType.REPONSE_CONFIG):
        return ConfigFrame.unpack(data_bytes)
    elif frame_type == int(DataFrameType.REPONSE_STATUS):
        return StatusFrame.unpack(data_bytes)
    elif frame_type == int(DataFrameType.ADC):
        return ADCDataFrame.unpack(data_bytes)
    else:
        raise FrameDecodeError("Unknown frame type %d" % frame_type)
    return None
