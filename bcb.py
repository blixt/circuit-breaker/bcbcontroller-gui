import logging
import time
import dataframe
import serialio


class BCBStatus(object):
    def __init__(self):
        self.is_on = False
        self.is_power_good = False
        self.is_ocp_triggered = False
        self.is_otp_triggered = False
        self.is_adc_dump_active = False


class BCBConfig(object):
    def __init__(self):
        self.adc_c1_zero = 0
        self.adc_c2_zero = 0
        self.adc_v_zero = 0

    def __str__(self):
        return "ADC_C1_ZERO %d, ADC_C2_ZERO %d, ADC_V_ZERO %d" % (self.adc_c1_zero,
                                                                  self.adc_c2_zero,
                                                                  self.adc_v_zero)


class BCBInterface(object):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.serial_io = serialio.SerialIO()
        self.serial_io.set_data_callback(self._on_data)
        self.connected = False
        self.callbacks = {}
        self.blocking_wait_frame = None
        self.cfg = None
        self.status = None
        self.adc_relative_tick = 0
        self.adc_last_tick32 = 0
        """ Callbacks """
        self.cb_measurement = None
        self.cb_measurement_raw = None
        self.cb_debug = None
        self.cb_status = None
        self.cb_error = None
        self.cb_connected = None
        self.cb_disconnected = None
        """ Parameters for adjusting ADC linearity """
        self.lin_adj_param_cm1_a = 1.121
        self.lin_adj_param_cm1_b = 0.003
        self.lin_adj_param_cm2_a = 1.112
        self.lin_adj_param_cm2_b = 0.002
        self.lin_adj_param_vm_a = 0.953
        self.lin_adj_param_vm_b = 0.099
        """ Other configurations """ 
        self.timestamp_clk = 2000000

    def set_timestamp_clk(self, clk_speed):
        self.timestamp_clk = clk_speed

    def set_lin_adj_params_cm1(self, a, b):
        self.lin_adj_param_cm1_a = a
        self.lin_adj_param_cm1_b = b

    def set_lin_adj_params_cm2(self, a, b):
        self.lin_adj_param_cm2_a = a
        self.lin_adj_param_cm2_b = b

    def set_lin_adj_params_vm(self, a, b):
        self.lin_adj_param_vm_a = a
        self.lin_adj_param_vm_b = b

    def connect(self, port, baudrate):
        self.ser_port = port
        self.ser_baudrate = baudrate
        try:
            self.serial_io.connect(port=port, baudrate=baudrate)
        except Exception as e:
            if self.cb_error is not None:
                self.cb_error(e)
            return
        self.logger.debug("Connected to %s", port)
        self.get_config()
        self.connected = True
        if self.cb_connected is not None:
            self.cb_connected()

    def disconnect(self):
        self.serial_io.disconnect()
        self.connected = False
        if self.cb_disconnected is not None:
            self.cb_disconnected()

    def is_connected(self):
        return self.connected

    def set_callback(self, name, callback):
        if name == 'measurement':
            self.cb_measurement = callback
        elif name == 'measurement_raw':
            self.cb_measurement_raw = callback
        elif name == 'status':
            self.cb_status = callback
        elif name == 'debug':
            self.cb_debug = callback
        elif name == 'error':
            self.cb_error = callback
        elif name == 'connected':
            self.cb_connected = callback
        elif name == 'disconnected':
            self.cb_disconnected = callback
        else:
            self.logger.error("Unknown callback %s", name)

    def reset(self, boot_flag=False):
        if (boot_flag):
            n_pulses = 4
            self.logger.info("Resetting to bootloader...")
        else:
            n_pulses = 8
            self.logger.info("Resetting ...")
        """
            When the DTR line is set for the first time, RTS line is also
            automatically set.
            However, both DTR and RTS lines can be set individually afterwards with out
            interfering to each other.
        """
        try:
            self.serial_io.set_dtr(1)
            for _ in range(n_pulses):
                time.sleep(0.001)
                self.serial_io.set_rts(1)
                time.sleep(0.001)
                self.serial_io.set_rts(0)
            time.sleep(0.001)
            self.serial_io.set_dtr(0)
        except Exception as e:
            if self.cb_error is not None:
                self.cb_error(e)
            return
        time.sleep(0.2)
        self.disconnect()
        self.connect(port=self.ser_port, baudrate=self.ser_baudrate)
        self.logger.info("Resetting done")

    def ocp_otp_reset(self):
        self.logger.debug("Sending COMMAND_OCP_OPT_RESET")
        cmd_frame = dataframe.CommandFrameNoArgs(dataframe.DataFrameType.COMMAND_OCP_OPT_RESET)
        self.serial_io.send(cmd_frame)

    def start_sampling(self):
        self.adc_relative_tick = 0
        self.adc_last_tick32 = 0
        self.logger.debug("Sending COMMAND_START_ADC_DUMP")
        cmd_frame = dataframe.CommandFrameNoArgs(dataframe.DataFrameType.COMMAND_START_ADC_DUMP)
        self.serial_io.send(cmd_frame)

    def stop_sampling(self):
        self.logger.debug("Sending COMMAND_STOP_ADC_DUMP")
        cmd_frame = dataframe.CommandFrameNoArgs(dataframe.DataFrameType.COMMAND_STOP_ADC_DUMP)
        self.serial_io.send(cmd_frame)

    def on(self):
        self.logger.debug("Sending COMMAND_ON")
        cmd_frame = dataframe.CommandFrameNoArgs(dataframe.DataFrameType.COMMAND_ON)
        self.serial_io.send(cmd_frame)

    def off(self):
        self.logger.debug("Sending COMMAND_OFF")
        cmd_frame = dataframe.CommandFrameNoArgs(dataframe.DataFrameType.COMMAND_OFF)
        self.serial_io.send(cmd_frame)

    def get_status(self, blocking=False, **kwargs):
        self.status = None
        self.logger.debug("Sending COMMAND_GET_STATUS")
        cmd_frame = dataframe.CommandFrameNoArgs(dataframe.DataFrameType.COMMAND_GET_STATUS)
        self.serial_io.send(cmd_frame)
        sent_time = round(time.time(), 1)

        if blocking:
            self.blocking_wait_frame = type(dataframe.StatusFrame)
            send_interval = 0.5
            while self.blocking_wait_frame is not None:
                # Send the get status command until the response is received
                now_time = round(time.time(), 1)
                if ((now_time - sent_time) >= send_interval):
                    sent_time = round(time.time(), 1)
                    self.logger.debug("Sending COMMAND_GET_STATUS")
                    self.serial_io.send(cmd_frame)
                self.poll()
                time.sleep(0.001)
            return self.status
        else:
            self.blocking_wait_frame = None
            return None

    def get_config(self, blocking=False):
        if self.status is None:
            # We need to update the status
            self.logger.debug("Status not updated")
            self.get_status(blocking=blocking)

        self.cfg = None
        self.logger.debug("Sending COMMAND_GET_CONFIG")
        cmd_frame = dataframe.CommandFrameNoArgs(dataframe.DataFrameType.COMMAND_GET_CONFIG)
        self.serial_io.send(cmd_frame)

        if blocking is True:
            self.blocking_wait_frame = type(dataframe.ConfigFrame)
            while self.blocking_wait_frame is not None:
                self.poll()
                time.sleep(0.001)
            return self.cfg
        else:
            self.blocking_wait_frame = None
            return None

    def calibrate(self, blocking=False):
        if self.status is None:
            # We need to update the status
            self.logger.debug("Status not updated")
            self.get_status(blocking=blocking)
        # Send calibration command
        self.logger.debug("Sending COMMAND_CALIBRATE")
        cmd_frame = dataframe.CommandFrameNoArgs(dataframe.DataFrameType.COMMAND_CALIBRATE)
        self.serial_io.send(cmd_frame)
        # We have get the new configuration after the calibration
        return self.get_config(blocking=blocking)

    def poll(self):
        try:
            self.serial_io.poll()
        except Exception as e:
            if self.cb_error is not None:
                self.cb_error(e)

    def _process_status_frame(self, status_frame):
        self.logger.debug("Received REPONSE_STATUS")
        self.status = BCBStatus()
        self.status.is_on = status_frame.is_on
        self.status.is_power_good = status_frame.is_power_good
        self.status.is_ocp_triggered = status_frame.is_ocp_triggered
        self.status.is_otp_triggered = status_frame.is_otp_triggered
        self.status.is_adc_dump_active = status_frame.is_adc_dump_active
        # Check if already waiting in blocking mode
        if self.blocking_wait_frame == type(dataframe.StatusFrame):
            self.blocking_wait_frame = None
        else:
            if self.cb_status is not None:
                self.cb_status(self.status)

    def _process_adc_frame(self, adc_frame):
        # Check if we've already received the configuration since without the configuration
        # we can not process ADC frame
        if self.cfg is None:
            return
        # Calculate current
        if len(adc_frame.channel_values) != 3:
            self.logger.warn("ADC frame should have 3 channels")
            return

        for id, val in adc_frame.channel_values:
            if id == 1:
                adc_c1 = val
            elif id == 2:
                adc_c2 = val
            elif id == 3:
                adc_v = val

        # Calculate current
        if adc_c2 < 4000 and adc_c2 > 200:
             # Use low range for accuracy
            if (adc_c2 < self.cfg.adc_c2_zero):
                sign = -1
                val = ((self.cfg.adc_c2_zero - adc_c2) * 5 * 1000000) >> 22
            else:
                sign = 1
                val = ((adc_c2 - self.cfg.adc_c2_zero) * 5 * 1000000) >> 22
            current = (float)(val * sign) / 1000
            current = (current - self.lin_adj_param_cm2_b) / self.lin_adj_param_cm2_a

        else:
            # low range is about to saturate, So we use the high range
            if (adc_c1 < self.cfg.adc_c1_zero):
                sign = -1
                val = ((self.cfg.adc_c1_zero - adc_c1) * 5 * 1000000) >> 18
            else:
                sign = 1
                val = ((adc_c1 - self.cfg.adc_c1_zero) * 5 * 1000000) >> 18
            current = (float)(val * sign) / 1000
            current = (current - self.lin_adj_param_cm1_b) / self.lin_adj_param_cm1_a

        # Caculate voltage
        if (adc_v < self.cfg.adc_v_zero):
            sign = -1
            val = (401 * (self.cfg.adc_v_zero - adc_v) * 5 * 1000) >> 13
        else:
            sign = 1
            val = (401 * (adc_v - self.cfg.adc_v_zero) * 5 * 1000) >> 13
        voltage = (float)(val * sign) / 1000
        voltage = (voltage - self.lin_adj_param_vm_b) / self.lin_adj_param_vm_a

        tick_diff = 0
        if self.adc_last_tick32 > adc_frame.time_tick:
            # Integer overflow happended
            tick_diff = (0xffffffff - self.adc_last_tick32) + adc_frame.time_tick
        else:
            tick_diff = adc_frame.time_tick - self.adc_last_tick32

        if self.adc_last_tick32 != 0:
            self.adc_relative_tick += tick_diff

        self.adc_last_tick32 = adc_frame.time_tick

        relative_timestamp = self.adc_relative_tick * 1000000000 / self.timestamp_clk 
        #relative_timestamp = float(self.adc_relative_tick) / self.timestamp_clk 

        if self.cb_measurement is not None:
            self.cb_measurement(relative_timestamp, current, voltage)

        if self.cb_measurement_raw is not None:
            self.cb_measurement_raw(relative_timestamp, adc_c1, adc_c2, adc_v)

    def _process_config_frame(self, config_frame):
        self.logger.debug("Received REPONSE_CONFIG")
        self.cfg = BCBConfig()
        self.cfg.adc_c1_zero = config_frame.c1_zero
        self.cfg.adc_c2_zero = config_frame.c2_zero
        self.cfg.adc_v_zero = config_frame.v_zero
        self.logger.debug("BCBConfig %s" % str(self.cfg))
        # Check if already waiting in blocking mode
        if self.blocking_wait_frame == type(dataframe.ConfigFrame):
            self.blocking_wait_frame = None
        else:
            pass

    def _process_debug_frame(self, debug_frame):
        if self.cb_debug is not None:
            self.cb_debug(str(debug_frame))

    def _on_data(self, data_frame):
        if isinstance(data_frame, dataframe.ADCDataFrame):
            self._process_adc_frame(data_frame)
        elif isinstance(data_frame, dataframe.ConfigFrame):
            self._process_config_frame(data_frame)
        elif isinstance(data_frame, dataframe.DebugPrintFrame):
            self._process_debug_frame(data_frame)
        elif isinstance(data_frame, dataframe.StatusFrame):
            self._process_status_frame(data_frame)
        else:
            self.logger.debug("Unhandled frame %s" % type(data_frame))

    @staticmethod
    def get_devices():
        return serialio.SerialIO.get_ports(hwid_filter=["0403:6015"])
