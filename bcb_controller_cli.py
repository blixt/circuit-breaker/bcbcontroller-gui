import sys
import argparse
import time
import logging
import bcb

logger = logging.getLogger(__name__)

def on_debug(line):
    sys.stdout.write(line)

def on_measurement(time_tick, current, voltage):
    print("T %d, I %d, V %d" % (time_tick, current, voltage))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Blixt Circuit Breaker Controller')
    parser.add_argument('port', metavar='PORT', type=str,
                        default="/dev/ttyUSB0", help='Serial port')
    parser.add_argument('-b', '--baudrate', type=int, default=115200,
                        help='Baudrate of the serial port')
    parser.add_argument('-C', '--calibrate', action='store_true',
                        default=False, help='Perform calibration')
    parser.add_argument('-G', '--gain-correct', action='store_true',
                        default=False, help='Perform ADC gain correction')
    parser.add_argument('-D', '--dump', action='store_true',
                        default=False, help='Start dumping measurements')
    parser.add_argument('-R', '--reset', action='store_true',
                        default=False, help='Reset the MCU')
    parser.add_argument('-d', '--debug', action='store_true',
                        default=False, help='Debug output')

    args = parser.parse_args()
    config = vars(args)

    if config['debug'] is True:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    bcb_if = bcb.BCBInterface()
    bcb_if.connect(port=config['port'], baudrate=config['baudrate'])
    bcb_if.set_callback('debug', on_debug)
    bcb_if.set_callback('measurement', on_measurement)

    if config['reset']:
        bcb_if.reset()

    bcb_if.get_status(blocking=True)

    if config['gain_correct']:
        logger.info("ADC Gain correcting")
        bcb_if.adc_gain_correct(blocking=True)
    
    if config['calibrate']:
        logger.info("Calibrating")
        bcb_if.calibrate(blocking=True)

    if config['dump']:
        bcb_if.get_config(blocking=True)
        bcb_if.start_sampling()
        try:
            while True:
                bcb_if.poll()
        except Exception as e:
            logger.error(e, exc_info=True)
            bcb_if.stop_sampling()
    bcb_if.disconnect()
